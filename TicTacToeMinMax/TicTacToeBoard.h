#pragma once
#include <iostream>
#include <vector>
#include <string>
const int ROW = 3;
const int COLUMN = 3;

class Board
{
	private:
		std::vector<std::vector<std::string>> board{3,std::vector<std::string>(3)};
		void GenerateBoard();

	public:
		Board();

	std::vector<std::vector<std::string>> getBoard();


	int getRow();

	int getColumn();
	bool makeMove(int row, int column, std::string &value);

	~Board();



};


class TicTacToeBoard
{
private:
	Board* myGameBoard;
	int moves = 0;
	
public:

	TicTacToeBoard();
	~TicTacToeBoard();

	int getMoves();
	bool MakeMove(int rowPos, int colPos, std::string value);
	bool hasChosen(int x, int y);

	void print();
	Board *getCurrentBoardState();

};