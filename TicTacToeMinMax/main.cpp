#include "TicTacToeBoard.h"
#include "MinMaxTree.h"
#include <iostream>

int main()
{
	TicTacToeBoard* board = new TicTacToeBoard();
	

	std::string user;
	std::string ai;
	std::cout << "Are you 'X' or 'O'? " << std::endl;
	std::cin >>  user;
	std::cout << user << std::endl;
	ai = (user == "X" ? "O" : "X");
	std::cout << std::endl;
	MinMaxTree* tree = new MinMaxTree(user,ai);
	while (board->getMoves()<9)
	{

		std::cout << "Enter your move row: ";
		PlayerMove move ;
		std::cin >> move.x;
		std::cout << std::endl;
		std::cout << "Enter your move column: ";
		std::cin >> move.y;
		std::cout << std::endl;
		bool result= board->MakeMove(move.x, move.y, user);
		while(!result)
		{
			std::cout << "Cannot play in an area thats been chosen" << std::endl;
			std::cout << "Enter your move row: ";
			std::cin >> move.x;
			std::cout << std::endl;
			std::cout << "Enter your move column: ";
			std::cin >> move.y;
			std::cout << std::endl;
			result = board->MakeMove(move.x, move.y, user);
		}


		PlayerMove aiMove = tree->BestMove(board);

		result=board->MakeMove(aiMove.x, aiMove.y, ai);

		board->print();

		
	}






	delete board;
	delete tree;
	system("Pause");
	return 0;
}
