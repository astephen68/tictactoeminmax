
#include "TicTacToeBoard.h"

TicTacToeBoard::TicTacToeBoard()
{
	myGameBoard = new Board();
}

TicTacToeBoard::~TicTacToeBoard()
{
	delete myGameBoard;
}

int TicTacToeBoard::getMoves()
{
	return moves;
}

bool TicTacToeBoard::MakeMove(int rowPos, int colPos, std::string value)
{
	bool result= myGameBoard->makeMove(rowPos, colPos, value);
	if (result == true)
	{
		moves++;
	}
	return result;
}

bool TicTacToeBoard::hasChosen(int x, int y)
{
	return((myGameBoard->getBoard()[x][y] == "_") ?false : true);
}

void TicTacToeBoard::print()
{
	for (int i = 0; i < myGameBoard->getRow(); i++)
	{
		std::cout << "[";
		for (int j = 0; j < myGameBoard->getColumn(); j++)
		{
			std::cout << myGameBoard->getBoard()[i][j];
		}
		std::cout << "]" << std::endl;
	}
}

Board *TicTacToeBoard::getCurrentBoardState()
{
	return myGameBoard;
}


void Board::GenerateBoard()
{
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COLUMN; j++)
		{
			board[i][j] = '_';
		}
	}
}

Board::Board()
{
	GenerateBoard();
}

std::vector<std::vector<std::string>> Board::getBoard()
{
	return board;
}

int Board::getRow()
{
	return ROW;
}

int Board::getColumn()
{
	return COLUMN;
}

bool Board::makeMove(int row, int column, std::string &value)
{
	
	if (board[row][column]=="_")
	{
		board[row][column] = value;
		return true;
	}
	return false;
	

}

Board::~Board()
{
}
