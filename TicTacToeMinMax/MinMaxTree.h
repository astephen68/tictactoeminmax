#pragma once
#include "TicTacToeBoard.h"
#include <algorithm>
#include <string>

struct PlayerMove
{
	int x;
	int y;
};
class MinMaxTree
{
private:
	int playerWinsValue = 1;
	int drawValue = 0;
	int opponentWinValue = 1;
	std::string player;
	std::string opponent;

public:
	MinMaxTree(std::string player, std::string opp);
	bool anyMovesLeft(std::vector<std::vector<std::string>> board);
	int boardConditon(std::vector<std::vector<std::string>> board);
	int V(std::vector<std::vector<std::string>> board, int depth, bool isMaxValue);
	PlayerMove BestMove(TicTacToeBoard* board);
	~MinMaxTree();
};