
#include "MinMaxTree.h"

MinMaxTree::MinMaxTree(std::string player, std::string opp)
{
	this->player = player;
	opponent = opp;
}

bool MinMaxTree::anyMovesLeft(std::vector<std::vector<std::string>> board)
{
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COLUMN; j++)
		{
			if (board[i][j]=="_")
			{
				return true;
			}
		}
	}

	return false;
	
}

int MinMaxTree::boardConditon(std::vector<std::vector<std::string>> ticTacToeBoard)
{

	for (int i = 0; i < ROW; i++)
	{

		//Row Check
		if ((ticTacToeBoard[i][0]== ticTacToeBoard[i][1])
			&& (ticTacToeBoard[i][1] == ticTacToeBoard[i][2]))
		{
			if (ticTacToeBoard[i][0] == player)
			{
				return +1;
			}
			else if (ticTacToeBoard[i][0] == opponent)
			{
				return -1;
			}
		}
	}
	//checking columns
	for (int j = 0; j < COLUMN; j++)
	{
		if ((ticTacToeBoard[0][j]==ticTacToeBoard[1][j])
			&&(ticTacToeBoard[1][j]==ticTacToeBoard[2][j] ))
		{
			if (ticTacToeBoard[0][j] == player)
			{
				return +1;
			}
			else if (ticTacToeBoard[0][j] == opponent)
			{
				return -1;
			}
		}
	}

	//checking diagionals
	if ((ticTacToeBoard[0][0] == ticTacToeBoard[1][1])
		&& (ticTacToeBoard[1][1] == ticTacToeBoard[2][2]))
	{
		if (ticTacToeBoard[0][0]==player)
		{
			return +1;
		}
		else if (ticTacToeBoard[0][0] == opponent)
		{
			return -1;
		}

	}
	if ((ticTacToeBoard[0][2]== ticTacToeBoard[1][1])
		&& (ticTacToeBoard[1][1] == ticTacToeBoard[2][0]))
	{
		if (ticTacToeBoard[0][2] == player)
		{
			return +1;
		}
		else if (ticTacToeBoard[0][2] == opponent)
		{
			return -1;
		}

	}

	return drawValue;

}

int MinMaxTree::V(std::vector<std::vector<std::string>> value, int depth, bool isMaxValue)
{
	int winvalue = boardConditon(value);

	if (winvalue==1)
	{
		return winvalue;
	}

	if (winvalue==-1)
	{
		return winvalue;
	}

	if (anyMovesLeft(value)==false)
	{
		return drawValue;
	}

	if (isMaxValue)
	{
		int bestValue = -1000;

		for (int i = 0; i <ROW; i++)
		{
			for (int j = 0; j < COLUMN; j++)
			{
				if (value[i][j]=="_")
				{
					value[i][j]= player;
					bestValue = std::max(bestValue, V(value, depth + 1, !isMaxValue));
					value[i][j]= "_";

				}
			}
		}

		return bestValue;
	}
	else
	{
		int bestValue = 1000;
		for (int i = 0; i < ROW; i++)
		{
			for (int j = 0; j < COLUMN; j++)
			{
				if (value[i][j] == "_")
				{
					value[i][j]= opponent;
					bestValue = std::min(bestValue, V(value, depth + 1, !isMaxValue));
					value[i][j] ="_";

				}
			}
		}
		return bestValue;
	}


}

PlayerMove MinMaxTree::BestMove(TicTacToeBoard * board)
{
	int bestValue = 1000;
	PlayerMove move;
	move.x = -1;
	move.y = -1;

	std::vector<std::vector<std::string>> myBoard = board->getCurrentBoardState()->getBoard();
	for (int i = 0; i < board->getCurrentBoardState()->getRow(); i++)
	{
		for (int j = 0; j < board->getCurrentBoardState()->getColumn(); j++)
		{
			if (myBoard[i][j] == "_")
			{
				myBoard[i][j] = opponent;

				int moveTotal = V(myBoard, 0, true);
				std::cout << "Current Move total: " << moveTotal << std::endl;
				myBoard[i][j] = "_";
				if (moveTotal<bestValue)
				{
					move.x = i;
					move.y = j;
					bestValue = moveTotal;

				}
			}
		}
	}
	return move;
}

MinMaxTree::~MinMaxTree()
{
}
